using System;

namespace Otus.Teaching.Pcf.Dictionary.MVCWebHost.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
