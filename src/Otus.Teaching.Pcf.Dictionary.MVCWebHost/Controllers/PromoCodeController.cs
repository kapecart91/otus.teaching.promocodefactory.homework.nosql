﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Dictionary.Services.DTOs;
using Otus.Teaching.Pcf.Dictionary.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.MVCWebHost.Controllers
{
    public class PromoCodeController : Controller
    {
        private readonly IGetPromoCodeService _getPromoCodeService;
        private readonly ILogger<HomeController> _logger;

        public PromoCodeController(IGetPromoCodeService getPromoCodeService,
                                     ILogger<HomeController> logger)
        {
            _logger = logger;
            _getPromoCodeService = getPromoCodeService;
        }

        // GET: PreferenceController
        public async Task<ActionResult> PromoCodes()
        {
            var promoCodes = await _getPromoCodeService.GetList();
            return View("PromoCodes", promoCodes);
        }

    }
}
