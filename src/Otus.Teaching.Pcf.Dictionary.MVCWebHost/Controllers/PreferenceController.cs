﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Dictionary.Services.DTOs;
using Otus.Teaching.Pcf.Dictionary.Services.Interfaces;
using Otus.Teaching.Pcf.Dictionary.Services.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.MVCWebHost.Controllers
{
    public class PreferenceController : Controller
    {
        private readonly IPreferenceService _preferenceService;
        private readonly ILogger<HomeController> _logger;

        public PreferenceController(IPreferenceService preferenceService,
                                     ILogger<HomeController> logger)
        {
            _logger = logger;
            _preferenceService = preferenceService;
        }

        // GET: PreferenceController
        public async Task<ActionResult> Preferences()
        {
            var proferences = await _preferenceService.GetList();
            return View("Preferences", proferences);
        }

        public ActionResult PartialPreferenceList()
        {
            return PartialView("PartialPreferenceList", _preferenceService.GetList());
        }

        // GET: PreferenceController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PreferenceController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([FromForm] PreferenceDTO dto)
        {
            try
            {
                await _preferenceService.Create(dto);
                return RedirectToAction("Preferences");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return View(dto);
            }
        }

        // GET: PreferenceController/Edit/5
        public async Task<ActionResult> Edit(Guid id)
        {
            var model = await _preferenceService.Get(id);
            return View(model);
        }

        // POST: PreferenceController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([FromForm] PreferenceDTO dto)
        {
            try
            {
                await _preferenceService.Update(dto);
                return RedirectToAction("Preferences");
            }
            catch
            {
                return View(dto);
            }
        }

        // GET: PreferenceController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PreferenceController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete([FromForm] PreferenceDTO dto)
        {
            try
            {
                await _preferenceService.Delete(dto);
                return RedirectToAction("Preferences");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return View(dto);
            }
        }
    }
}
