﻿using Newtonsoft.Json;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class DictionaryGateway : IDictionaryGateway
    {
        private readonly HttpClient _httpClient;

        public DictionaryGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<List<Preference>> GetPromocodesAsync()
        {
            var response = await _httpClient.GetAsync($"api/Preference");

            var contents = await response.Content.ReadAsStringAsync();
            var model = JsonConvert.DeserializeObject<List<Preference>>(contents);

            return model;
        }
    }
}
