﻿using Otus.Teaching.Pcf.Dictionary.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.DTOs
{
    public class PromoCodeDTO : BaseDTO
    {
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public Guid PartnerId { get; set; }

        public PartnerDTO Partner { get; set; }

        public Guid PreferenceId { get; set; }

        public virtual PreferenceDTO Preference { get; set; }
    }
}
