﻿using Otus.Teaching.Pcf.Dictionary.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.DTOs
{
    public class PreferenceDTO : BaseDTO
    {
        public PreferenceDTO()
        {

        }

        public PreferenceDTO(Preference preference)
        {
            Id = preference.Id;
            Name = preference.Name;
        }

        [Display(Name = "Название")]
        [Required(ErrorMessage = "Поле обязательное для заполнения"), StringLength(50)]
        public string Name { get; set; }

        public static implicit operator Preference(PreferenceDTO dto)
        {
            return new Preference()
            {
                Id = dto.Id,
                Name = dto.Name
            };
        }
    }
}
