﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.Interfaces
{
    public interface ICRUD<DTO>
    {
        Task<IEnumerable<DTO>> GetList();

        Task<DTO> Get(Guid id);

        Task Create(DTO model);

        Task Update(DTO model);

        Task Delete(DTO model);

    }
}
