﻿using Otus.Teaching.Pcf.Dictionary.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.Interfaces
{
    public interface IPreferenceService : ICRUD<PreferenceDTO>
    {
    }
}
