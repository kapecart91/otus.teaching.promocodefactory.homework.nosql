﻿using Otus.Teaching.Pcf.Dictionary.Services.DTOs.PromoCodeDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.Interfaces
{
    public interface IGetPromoCodeService
    {
        Task<IEnumerable<GetPromoCodeDTO>> GetList();
    }
}
