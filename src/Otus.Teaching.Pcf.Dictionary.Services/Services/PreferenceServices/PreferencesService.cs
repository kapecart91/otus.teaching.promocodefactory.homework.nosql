﻿using Otus.Teaching.Pcf.Dictionary.Core.Domain;
using Otus.Teaching.Pcf.Dictionary.DataAccess;
using Otus.Teaching.Pcf.Dictionary.Services.DTOs;
using Otus.Teaching.Pcf.Dictionary.Services.Interfaces;
using Otus.Teaching.Pcf.Dictionary.Services.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.PreferenceServices
{
    public class PreferencesService : IPreferenceService
    {
        private readonly ICacheService<Preference> _preferences;

        public PreferencesService(ICacheService<Preference> preferences)
        {
            _preferences = preferences;
        }

        public async Task<IEnumerable<PreferenceDTO>> GetList()
        {
            var preferences = await _preferences.GetListAsync();

            return preferences.Select(x => new PreferenceDTO(x));
        }

        public async Task<PreferenceDTO> Get(Guid id)
        {
            Preference model = await Validation(id);

            return new PreferenceDTO(model);
        }

        public async Task Create(PreferenceDTO model)
        {
            await _preferences.AddAsync(model);
        }

        public async Task Update(PreferenceDTO model)
        {
            await Validation(model.Id);

            await _preferences.UpdateAsync(model);
        }

        public async Task Delete(PreferenceDTO model)
        {
            await Validation(model.Id);

            await _preferences.RemoveAsync(model);
        }

        private async Task<Preference> Validation(Guid id)
        {
            var model = await _preferences.GetByIdAsync(id);

            if (model == null)
            {
                throw new EntityIsNotFoundException("Запись предпочтений отсутствует!");
            }

            return model;
        }
    }
}
