﻿using Otus.Teaching.Pcf.Dictionary.Core.Domain;
using Otus.Teaching.Pcf.Dictionary.DataAccess;
using Otus.Teaching.Pcf.Dictionary.Services.DTOs;
using Otus.Teaching.Pcf.Dictionary.Services.DTOs.PromoCodeDTOs;
using Otus.Teaching.Pcf.Dictionary.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Services.Services.PromoCodeServices
{
    public class GetPromoCodeService : IGetPromoCodeService
    {
        private readonly ICacheService<PromoCode> _promocodes;
        private readonly ICacheService<Preference> _preferences;

        public GetPromoCodeService(ICacheService<PromoCode> promocodes,
                                    ICacheService<Preference> preferences)
        {
            _promocodes = promocodes;
            _preferences = preferences;

        }

        public async Task<IEnumerable<GetPromoCodeDTO>> GetList()
        {
            var promoCodes = await _promocodes.GetListAsync();
            var preferences = await _preferences.GetListAsync();

            return promoCodes.Select(x => new GetPromoCodeDTO(x)
            {
                PreferenceName = preferences.FirstOrDefault(p => p.Id == x.PreferenceId)?.Name
            });
        }

    }
}
