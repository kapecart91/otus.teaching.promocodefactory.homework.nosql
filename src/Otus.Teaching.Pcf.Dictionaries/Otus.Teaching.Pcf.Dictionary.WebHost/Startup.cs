using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Otus.Teaching.Pcf.Dictionary.DataAccess;
using Otus.Teaching.Pcf.Dictionary.DataAccess.Data;
using StackExchange.Redis;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            services.AddScoped(typeof(ICacheService<>), typeof(CacheService<>));
            services.AddScoped<IDbInitializer, RedisDbinitializer>();

            var configuration = Environment.GetEnvironmentVariable("Redis");
            Console.WriteLine(configuration);

#if DEBUG
            configuration = Configuration["Redis:Configuration"];
#endif

            if (configuration == null)
            {
                configuration = "localhost:6379";
            }

            Console.WriteLine(configuration);

            ConfigurationOptions optionCash = new ConfigurationOptions
            {
                AbortOnConnectFail = false,
                EndPoints = { configuration }
            };

            services.AddStackExchangeRedisCache(option =>
            {
                option.Configuration = configuration;
                option.ConfigurationOptions = optionCash;
            });

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory Dictionary API Doc";
                options.Version = "1.0";
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer initDb)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOpenApi();

            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            initDb.InitializeDb();

        }
    }
}
