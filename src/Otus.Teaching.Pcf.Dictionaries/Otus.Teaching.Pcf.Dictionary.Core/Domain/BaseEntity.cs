﻿using System;

namespace Otus.Teaching.Pcf.Dictionary.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}