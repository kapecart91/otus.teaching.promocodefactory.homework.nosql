﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.Core.Domain
{
    public enum UserType
    {
        /// <summary>
        /// Работник
        /// </summary>
        Employee,

        /// <summary>
        /// Партнер
        /// </summary>
        Partner
    }
}
