﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}
