﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.Dictionary.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.DataAccess.Data
{
    public class RedisDbinitializer : IDbInitializer
    {
        private readonly IDistributedCache _cache;

        public RedisDbinitializer(IDistributedCache cache)
        {
            _cache = cache;
        }

        public void InitializeDb()
        {
            var options = new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(1),
                SlidingExpiration = TimeSpan.FromMinutes(10)
            };

            _cache.SetString(nameof(Preference), JsonConvert.SerializeObject(FakeDataFactory.Preferences), options);

        }
    }
}
