﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.DataAccess
{
    public interface ICacheService<T>
    {
        Task<T> GetByIdAsync(Guid id);

        Task<T> AddAsync(T value);

        Task<T> RemoveAsync(T value);

        Task<T> UpdateAsync(T value);

        Task<List<T>> GetListAsync();
    }
}
