﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.Dictionary.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Dictionary.DataAccess
{
    public class CacheService<T> : ICacheService<T> where T : BaseEntity
    {
        private readonly IDistributedCache _cache;
        private readonly string _name;

        public CacheService(IDistributedCache cache)
        {
            _cache = cache;
            _name = typeof(T).Name;
        }

        public async Task<List<T>> GetListAsync()
        {

            var strCollection = await _cache.GetStringAsync(_name);

            if (strCollection == null)
            {
                return new List<T>();
            }

            var collection = JsonConvert.DeserializeObject<List<T>>(strCollection);

            return collection;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var collection = await GetListAsync();
            var item = collection.FirstOrDefault(x => x.Id == id);

            if (item != null)
            {
                return item;
            }

            return default;
        }

        public async Task<T> AddAsync(T value)
        {
            var options = new DistributedCacheEntryOptions
            {
                //AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(1),
               // SlidingExpiration = TimeSpan.FromMinutes(10)
            };

            var collection = await GetListAsync();
            value.Id = Guid.NewGuid();

            collection.Add(value);
            _cache.SetString(_name, JsonConvert.SerializeObject(collection), options);

            return value;
        }

        public async Task<T> RemoveAsync(T value)
        {
            var options = new DistributedCacheEntryOptions
            {
                //AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(1),
               // SlidingExpiration = TimeSpan.FromMinutes(10)
            };

            var collection = await GetListAsync();

            var item = collection.FirstOrDefault(x => x.Id == value.Id);

            if (item == null)
            {
                throw new NullReferenceException("Объект не найден  в базе данных");
            }

            collection.Remove(item);

            _cache.SetString(_name, JsonConvert.SerializeObject(collection), options);

            return value;
        }

        public async Task<T> UpdateAsync(T value)
        {
            var options = new DistributedCacheEntryOptions
            {
                //AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(1),
               // SlidingExpiration = TimeSpan.FromMinutes(10)
            };

            var collection = await GetListAsync();

            var item = collection.FirstOrDefault(x => x.Id == value.Id);

            if (item == null)
            {
                throw new NullReferenceException("Объект не найден  в базе данных");
            }

            collection.Remove(item);
            collection.Add(value);

            await _cache.SetStringAsync(_name, JsonConvert.SerializeObject(collection), options);

            return value;
        }
    }
}
